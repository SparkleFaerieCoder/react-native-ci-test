jest.mock('react-native-device-info', () => {
    return {
      getApplicationName: jest.fn(),
      getBrand: jest.fn()
    };
  });